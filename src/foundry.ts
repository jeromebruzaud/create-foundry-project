#!/usr/bin/env node
import program from 'commander';
import chalk from 'chalk';

import createProject from './scripts/createProject';

let projectName: string | undefined;

program
	.version(require('../package.json').version, '-v, --version', 'Checks the current version of Foundry Project Creator')
	.arguments('<project-directory>')
	.usage(`${chalk.green('<project-directory>')} [options]`)
	.action((name) => {
		projectName = name;
	})
	.option('-s, --system', 'Create a system instead of a module')
	.option('-t, --typescript', 'Configures the project to use TypeScript')
	.option('-l, --lint', 'Configures the project to use linting')
	.option('--css <preprocessor>', 'Configures the project to use a CSS preprocessor ("less", "sass")')
	.option('-f, --force', 'Overwrite an existing project directory')
	.option('--no-deps', 'Skip installing project dependencies')
	.option('--no-git', 'Skip initializing Git repository');

program.parse(process.argv);

if (typeof projectName === 'undefined') {
	console.error(chalk.red('Please specify a project directory'));
	program.help();
}

createProject(
	projectName as string,
	program.system,
	program.typescript,
	program.css,
	program.lint,
	program.force,
	program.deps,
	program.git
);
