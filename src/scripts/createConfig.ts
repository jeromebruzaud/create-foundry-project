import fs from 'fs-extra';
import path from 'path';
import ora from 'ora';
import chalk from 'chalk';

export default async (options: ProjectOptions) => {
	const spinner = ora('Creating configuration files').start();

	let configFile;
	let configPath;

	if (options.useTypeScript) {
		configFile = path.resolve(__dirname, '../..', 'template', 'config', 'tsconfig.json');
		configPath = path.join(options.dir, 'tsconfig.json');

		await fs.copy(configFile, configPath, { recursive: true });
	}

	if (options.useLint) {
		configFile = path.resolve(__dirname, '../..', 'template', 'config', '.eslintrc');
		configPath = path.join(options.dir, '.eslintrc');

		await fs.copy(configFile, configPath, { recursive: true });
	}

	if (options.deps) {
		const gulpFile = path.resolve(__dirname, '../..', 'template', 'config', 'gulpfile.js');
		const gulpFilePath = path.join(options.dir, 'gulpfile.js');
		configPath = path.join(options.dir, 'foundryconfig.json');

		const configJson = {
			dataPath: '',
			repository: '',
			rawURL: ''
		};
		
		await fs.writeJSON(configPath, configJson, { spaces: 2 });
		await fs.copy(gulpFile, gulpFilePath, { recursive: true });
	}

	spinner.succeed(chalk.green('Created configuration files'));
};
