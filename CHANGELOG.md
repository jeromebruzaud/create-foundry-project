# Changelog

## 1.5.1

### Added

-   The `lang` folder is now being watched and copied. It is considered part of the JS/TS workflow, but currently needs to be created manually.

### Fixed

-   Now _actually_ fixed the issue of Gulp `package` task finishing before the zip file has been written.
-   (_Uber facepalm_) Fixed a mistake in the `publish` task where incrementing a patch actually grabbed and incremented the minor number and inserted it as the patch number. _Why oh why did this slip through._

## 1.5.0

### Changed

-   Now that Foundry supports symbolic links, the `copy` task has been renamed to `link`. It now creates a symbolic link to the `dist` folder, instead of copying the files to the User Data folder.
-   Per the above change, the `watch` task no longer watches the `dist` folder for changes as the symbolic link allows Foundry to pick up the changes directly.
-   When the CLI creates the manifest JSON, it now adds a bunch of new properties (`authors`, `keywords`, `readme`, `license`, `bugs`, and `compatibleCoreVersion`). As usual, you may (and should) remove any unused properties.

### Fixed

-   Supposedly certain Gulp tasks wouldn't complete correctly due to incorrect use of async functions, notably `package`. This could cause the `publish` tasks to commit changes to the .zip package _while_ it is still being written, resulting in an 'unexpected end of archive' error. This should now be fixed.

## 1.4.0

### Added

-   `--no-git` option to skip initializing a Git repository.
-   `assets` folder to store static assets for styling, such as images.

### Fixed

-   `build:watch` did not watch for changes in subfolders of `src`.
-   Infinite loop while initializing Git when folder is part of an existing worktree.

## 1.3.1

### Changed

-   Replaced Node Sass compiler with Dart Sass to support mod

### Fixed

-   Gulp `watch` task not properly watching for LESS/Sass changes in subfolders.

## 1.3.0

### Added

-   Gulp `build` task now transforms TypeScript import declarations to append the `.js` extension.

### Fixed

-   Gulp `build` task was not cleaning the `dist` folder first, as the old scripts did.

## 1.2.0

### Changed

-   Switched to Gulp for automating building, packaging, and releasing projects.
-   Added `gulpfile.js` to the template files.
-   Completely removed Scripts package dependency as all functionality has been moved to use Gulp.

## 1.1.0

### Changed

-   Moved workspace configuration from `package.json` to `foundryconfig.json`. This file should not be committed to source control.
-   Removed `manifest` config option as the scripts will now look for the _source_ file in the `dist` (vanilla JS) and `src` (TypeScript) folders.
-   Added `update` script in `package.json` that quickly reinstalls the packages.

### Fixed

-   Scripts and Types packages are now installed correctly. They can be updated by running `npm run update`, which simply reinstalls them.

## 1.0.2

### Added

-   Added `manifest` config option in the created `package.json` to determine where the source manifest JSON is located (`dist` for vanilla JS, or `src` for TypeScript).

### Changed

-   Removed old `releaseBranch` config in the created `package.json` that is no longer used.

## 1.0.1 (January 12, 2020)

-   Full release

### Added

-   Config settings used by the scripts package added to `package.json` (see the [Foundry Project Creator Scripts](https://gitlab.com/foundry-projects/foundry-pc/foundry-pc-scripts) repository for more information).
-   `--no-deps` option to prevent installing scripts and type definitions dependencies.
-   `-l, --lint` option to include a (currently empty) `.eslintrc` config file to use ESLint.

### Changed

-   Project structure now adapts to the tools used (e.g. TypeScript and/or CSS preprocessor).
-   `.gitignore` does not ignore filders/folders in the `dist` folder if they contain source code (i.e. are not built from TypeScript or Less/SASS).
-   `tsconfig.json` is now a template file copied to the project folder, rather than included in the scripts package.

## 0.2.0

### Added

-   README with a simple explanation for how to use the tool.

### Changed

-   Removed old `init` command to instead make the tool a single command (future scripts will use a dev dependency). Use `foundry <directory>` instead of `foundry init <directory>`.

## 0.1.0 (November 28, 2019)

-   Initial release
